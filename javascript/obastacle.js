'use strict';

class Obstacle {

	constructor(context, posX, posY) { 

		this.context = context; 
		this.width = 10;
		this.height = 10;
		this.posX = posX;
		this.posY = posY;

	}

	moveLeft() { this.posX = this.posX - 1; }
	moveRight() { this.posX = this.posX + 1; }

	touch(player) {

		let playerLeft =  player.posX,
			playerRight = player.posX + player.width,
			playerTop = player.posY,
			playerBottom = player.posY + player.height;

		let obstacleLeft = this.posX,
            obstacleRight = this.posX+this.blockWidth,
            obstacleTop = this.posY,
            obstacleBottom = this.posY+this.blockHeight;

        if( characterLeft<=obstacleRight &&
            obstacleLeft<=characterRight &&
            characterTop<=obstacleBottom &&
            obstacleTop<=characterBottom)  
                return true;
        else 
        	return false;
	}
}

class Lava extends Obstacle {

	draw() {
	
		context.fillStyle = 'red';
        this.context.fillRect(this.posX, this.posY, this.width, this.height);
	}
}

class Toxic extends Obstacle {

	draw() {
	
		context.fillStyle = 'green';
        this.context.fillRect(this.posX, this.posY, this.width, this.height);
	}
}

