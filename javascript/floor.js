class Floor {
	constructor(posX, posY, blockWidth, blockHeight) {
    
    	this.posX = posX;
    	this.posY = posY;
    	this.speedX = componentSpeedX;
    	this.blockWidth = blockWidth;
    	this.blockHeight = blockHeight;
    }
	
	draw(context) {

        context.fillStyle = "#7f6215";
        context.fillRect(this.posX, this.posY, this.blockWidth, this.blockHeight);
        context.fillStyle = "#36c01a";
        context.fillRect(this.posX, this.posY, this.blockWidth, 5);
    }

    move() {

        if(character.firstBottomHit && character.canMove) 
                this.posX -= this.speedX;
    }
}