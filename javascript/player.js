'use strict';

class Player {

	constructor(context, posX, posY) { 

		this.context = context; 
		this.width = 10;
		this.height = 10;
		this.posX = posX;
		this.posY = posY;

	}

	draw() {
	
		this.context.fillStyle = 'red';
        this.context.fillRect(this.posX, this.posY, this.width, this.height);
	}

	moveLeft() { this.posX = this.posX - 1; }
	moveRight() { this.posX = this.posX + 1; }
}