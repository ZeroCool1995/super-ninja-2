window.addEventListener("keydown", function(e) {

	if(e.key == "ArrowLeft" || e.key == "a") game.player.moveLeft();
	else if(e.key == "ArrowRight" || e.key == "d") game.player.moveRight();
	
   }, false);

window.addEventListener("resize", function() {
	game.resize();
});
