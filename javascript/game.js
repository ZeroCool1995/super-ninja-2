'use strict';

class Game {

	constructor() { 

		this.canvas = document.getElementById('app');
		this.context = this.canvas.getContext('2d'); 
		this.width = 1024;
		this.height = 768;
		this.posX = 0;
		this.posY = 0;

		this.ratio = this.width / this.height;
		this.currWidth = this.width;
		this.currHeight = this.height;

		this.player = new Player(this.context, 10, 10);
	}

	resize() {

		this.currHeight = window.innerHeight;
		this.currWidth = this.currHeight * this.ratio;

		let device = navigator.userAgent.toLowerCase();
		let android = device.indexOf('android') > -1 ? true : false;
		let ios = ( device.indexOf('iphone') > -1 || device.indexOf('ipad') > -1 ) ? true : false;

		if( android || ios )
			document.body.style.height = (window.innerheight + 50) + 'px';

		this.canvas.style.width = this.currWidth + 'px';
		this.canvas.style.height = this.currHeight + 'px';

		this.canvas.width = this.currWidth;
		this.canvas.height = this.currHeight;
	}

	clear() { this.context.clearRect(0, 0, this.width, this.height); }

	drawFrame() {
	
		this.player.draw();
	}
}

let game = new Game();
game.resize();

let requestId = 0;

window.requestAnimationFrame = window.requestAnimationFrame || window.msRequestAnimationFrame;

(function loop(timestamp) {

  game.clear();
  game.drawFrame();
  requestId = window.requestAnimationFrame(loop);
})();